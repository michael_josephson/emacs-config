;;; init.el -- Initialization
;;; Time-stamp: <2016-07-07 23:26:11 (michael)>
;;; Author: Michael Josephson
;;;
;;; Commentary:
;;;

;;; Code:
;; Global variables
(setq user-home-directory     (concat (getenv "HOME") "/")) ; must end with /
(setq user-emacs-directory    (concat user-home-directory ".emacs.d/")) ; must end with /
(setq setup-packages-file     (locate-user-emacs-file "setup-packages.el"))
(setq emacs-version-short     (replace-regexp-in-string
                               "\\([0-9]+\\)\\.\\([0-9]+\\).*"
                               "\\1_\\2" emacs-version)) ; 25.0.50.1 -> 25_0
(setq custom-file             (locate-user-emacs-file
                               (concat ".custom.el")))

;; Package Management
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives '("elpy" . "http://jorgenschaefer.github.io/packages/"))
(add-to-list 'load-path (concat user-emacs-directory "setup-files/"))
(add-to-list 'load-path (concat user-emacs-directory "elisp/"))
(add-to-list 'load-path (concat user-emacs-directory "elisp/use-package/"))
(add-to-list 'load-path (concat user-emacs-directory "elisp/sme-mode/"))
(add-to-list 'load-path (concat user-emacs-directory "elisp/styleguide"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
;; End Package Management

(eval-when-compile
  (require 'use-package))
(require 'diminish)
(require 'bind-key)

;; Basic requires
(require 'cl)
(require 'cl-lib)
(require 'use-package)
(require 'setup-paradox)
;; End of basic requires

;; Look and feel setup
(require 'setup-visual)
(require 'setup-popwin)
(require 'setup-editing)

;; Package setup
(require 'setup-aindent)
(require 'setup-anzu)
(require 'setup-comment-dwim-2)
(require 'setup-company)
(require 'setup-dtrt-indent)
(require 'setup-duplicate-thing)
(require 'setup-helm)
(require 'setup-header2)
(require 'setup-iedit)
(when (executable-find "git")
  (require 'setup-git))
;;(require 'setup-jabber)
(require 'setup-markdown)
(require 'setup-multi-term)
(require 'setup-projectile)
(require 'setup-smartparens)
;;(require 'setup-sme)
(require 'setup-undotree)
(require 'setup-volatile-highlights)
(require 'setup-wsbutler)
(require 'setup-rx)
(require 'setup-yasnippet)

;; Languages setup
(require 'setup-helm-gtags)
(require 'setup-flycheck)
(require 'setup-company-jedi)
;;(require 'setup-js2mode)
;;(require 'setup-sassmode)
(require 'setup-webmode)
;;(require 'setup-cedet)
;;(require 'setup-elpy)
(require 'setup-linum)

;; Email
;;(require 'setup-email)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
(require 'setup-misc)

(load custom-file :noerror :nomessage)

(eval-after-load 'tramp '(setenv "SHELL" "/bin/bash"))

(setq emacs-initialized t)
(provide 'init)
;;; init.el ends here

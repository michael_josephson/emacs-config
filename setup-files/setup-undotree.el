;; Time-stamp: <2015-08-11 17:31:03 (michael)>


(use-package undo-tree
  :ensure t
  :config
  (global-undo-tree-mode))

(provide 'setup-undotree)

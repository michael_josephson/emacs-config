;; Time-stamp: <2015-08-12 10:08:09 (michael)>

;; header2
;; http://www.emacswiki.org/emacs/header2.el

(use-package header2
  :ensure t
  :config
  (autoload 'auto-update-file-header "header2")
  (add-hook 'write-file-hooks 'auto-update-file-header)
  (autoload 'auto-make-header "header2")
  (add-hook 'emacs-lisp-mode-hook 'auto-make-header)
  (add-hook 'c-mode-common-hook   'auto-make-header))

(provide 'setup-header2)

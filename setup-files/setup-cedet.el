;; Time-stamp: <2015-08-18 13:17:59 (michael)>
;;
;; C++ Setup


(use-package cc-mode
  :ensure t
  :config
  (use-package semantic
    :ensure t
    :init
    (setq global-semanticdb-minor-mode 1)
    (setq global-semantic-idle-scheduler-mode 1)
    (setq global-semantic-stickyfunc-mode 1)
    :config
    (progn
      (defun alexott/cedet-hook ()
        (local-set-key "\C-c\C-j" 'semantic-ia-fast-jump)
        (local-set-key "\C-c\C-s" 'semantic-ia-show-summary))
      (add-hook 'c-mode-common-hook 'alexott/cedet-hook)
      (add-hook 'c-mode-hook 'alexott/cedet-hook)
      (add-hook 'c++-mode-hook 'alexott/cedet-hook)
      )
    (semantic-mode 1))
  (use-package ede
    :ensure t
    :init
    ;;(setq c-default-style "linux")
    (setq gdb-many-windows t)
    (setq gdb-show-main t)
    (add-hook 'c-mode-common-hook 'hs-minor-mode)
    :config
    (progn
      (global-set-key (kbd "<f5>") (lambda ()
                                     (interactive)
                                     (setq-local compilation-read-command nil)
                                     (call-interactively 'compile))))
    (global-ede-mode)))

;; Use Google style guide
(require 'google-c-style)
(add-hook 'c-mode-hook
      (lambda()
            (subword-mode)
            (google-set-c-style)
            (google-make-newline-indent)
            (setq c-basic-offset 2)))

(provide 'setup-cedet)

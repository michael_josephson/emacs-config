;; Time-stamp: <2015-08-11 17:04:03 (michael)>

;; Package: ws-butler

(use-package ws-butler
  :ensure t
  :init
  (add-hook 'prog-mode-hook 'ws-butler-mode)
  (add-hook 'c-mode-common-hook 'ws-butler-mode)
  (add-hook 'text-mode 'ws-butler-mode)
  (add-hook 'fundamental-mode 'ws-butler-mode))

(provide 'setup-wsbutler)

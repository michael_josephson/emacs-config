;; Time-stamp: <2015-08-26 13:51:29 (michael)>
;;


;; Time stamp setup
(setq 
  time-stamp-active t                                     ; enable time-stamps
  time-stamp-line-limit 10                                ; check first 10 buffer lines for Time-stamp: 
  time-stamp-format "%04y-%02m-%02d %02H:%02M:%02S (%u)") ; date format
(add-hook 'write-file-hooks 'time-stamp)                  ; update when saving

(setq global-mark-ring-max 5000         ; increase mark ring to contains 5000 entries
      mark-ring-max 5000                ; increase kill ring to contains 5000 entries
      mode-require-final-newline t      ; add a newline to end of file
      tab-width 4                       ; default to 4 visible spaces to display a tab
      )

(add-hook 'sh-mode-hook (lambda ()
                          (setq tab-width 4)))

(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-language-environment "UTF-8")
(prefer-coding-system 'utf-8)

(setq-default indent-tabs-mode nil)
(delete-selection-mode)

(setq kill-ring-max 5000 ; increase kill-ring capacity
      kill-whole-line t  ; if NIL, kill whole line and move the next line up
      )

;; show whitespace in diff-mode
(add-hook 'diff-mode-hook (lambda ()
                            (setq-local whitespace-style
                                        '(face
                                          tabs
                                          tab-mark
                                          spaces
                                          space-mark
                                          trailing
                                          indentation::space
                                          indentation::tab
                                          newline
                                          newline-mark))
                            (whitespace-mode 1)))


;; Backups

;; When `vc-make-backup-files' is nil (default), backups are not made for
;; version controlled (e.g. git) files. Set this to `t' to allow making backups
;; using prefix args to `save-buffer' command.
(setq vc-make-backup-files t)

;; http://www.gnu.org/software/emacs/manual/html_node/elisp/Numbered-Backups.html
;; http://stackoverflow.com/a/151946/1219634
;; `version-control' is nil by default; numbered backups are made only if the
;; the visited file already has numbered backups. Below will always create
;; numbered backups.
(setq version-control :make-numbered-backups)

(setq kept-new-versions 4) ; default 2
(setq kept-old-versions 2) ; default 2
;; If there are backups numbered 1, 2, 3, 5, and 7, and both of the above
;; variables have the value 2, then the backups numbered 1 and 2 are kept
;; as old versions and those numbered 5 and 7 are kept as new versions;
;; backup version 3 is deleted after user confirmation.

;; Excess backup deletion will happen silently, without user confirmation, if
;; `delete-old-versions' is set to `t'.
(setq delete-old-versions t) ; default nil

;; http://www.gnu.org/software/emacs/manual/html_node/elisp/Rename-or-Copy.html
(setq backup-by-copying t) ; don't clobber symlinks

(setq mjj/backup-directory
      (let ((dir (concat (getenv "HOME") "/.saves/"))) ; must end with /
        (make-directory dir :parents)
        dir))

;; Save all backups to `mjj/backup-directory'
(setq backup-directory-alist `(("." . ,mjj/backup-directory)))
(message (format "All backup files will be saved to %s." mjj/backup-directory))

(global-set-key (kbd "C-h") 'delete-backward-char)
(global-set-key (kbd "M-h") 'delete-backward-word)

(provide 'setup-editing)

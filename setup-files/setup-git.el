;; Time-stamp: <2015-08-11 13:57:34 (michael)>

;; Git Utilities

(use-package git-link
  :ensure t
  :config
  (progn
    (defun git-link-force-hash ()
      "This function is the same as `git-link' except that it copies the link
with the commit hash in the link even if branch name is available."
      (interactive)
      (let ((git-link-use-commit t))
        (call-interactively 'git-link)))
    (when (featurep 'region-bindings-mode)
      (bind-keys
       :map region-bindings-mode-map
       ("g" . git-link-force-hash)))))


(use-package git-timemachine
  :ensure t)

(use-package magit
  :ensure t
  :bind ("C-x g" . magit-status))

(provide 'setup-git)

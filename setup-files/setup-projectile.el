;; Time-stamp: <2015-08-11 16:17:27 (michael)>

;; Projectile

(use-package projectile
  :ensure t
  :init (setq projectile-enable-caching t)
  :config
  (use-package helm-projectile
    :ensure t
    :init
    (setq projectile-completion-system 'helm)
    (setq projectile-indexing-method 'alien)
    :config
    (helm-projectile-on))
  (projectile-global-mode))

(provide 'setup-projectile)

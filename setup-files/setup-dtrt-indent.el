;; Time-stamp: <2015-08-11 13:08:46 (michael)>
;; dtrt-indent

;; PACKAGE: dtrt-indent
;;(require 'dtrt-indent)
;;(dtrt-indent-mode 1)
;;(setq dtrt-indent-verbosity 0)

(use-package dtrt-indent
  :ensure t
  :defer t
  :config
  (dtrt-indent-mode 1)
  (setq dtrt-indent-verbosity 0))

(provide 'setup-dtrt-indent)

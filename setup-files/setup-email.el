;;; setup-email.el ---
;;; Time-stamp: <2016-07-07 23:24:09 (michael)>
;;; Author: Michael Josephson
;;;
;;; Commentary:
;;;

;;; Code:

(setq load-path (cons (expand-file-name "~/projects/mu/mu4e") load-path))

(require 'mu4e)

(setq mu4e-maildir (expand-file-name "~/.mail"))

(setq mu4e-sent-folder   "/Sent")
(setq mu4e-drafts-folder "/Drafts")
(setq mu4e-trash-folder  "/Trash")

(setq
 mu4e-get-mail-command "offlineimap"
 mu4e-update-interval 300)

(setq
 message-send-mail-function 'smtpmail-send-it
 smtpmail-smtp-server "mail.simlabdevelopments.com" )

(setq mu4e-view-show-images t)

(provide 'setup-email)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; setup-email.el ends here

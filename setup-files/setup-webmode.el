;; Time-stamp: <2016-07-07 22:21:55 (michael)>
;;
;; Web-mode Setup


(use-package web-mode
  :ensure t
  :mode (("\\.html?\\'" . web-mode)
         ("\\.scss\\'" . web-mode)
        )
  :init
  (setq web-mode-markup-indent-offset 2
        web-mode-css-indent-offset 2
        web-mode-code-indent-offset 2
        web-mode-style-padding 2
        web-mode-script-padding 2)
)

(provide 'setup-webmode)

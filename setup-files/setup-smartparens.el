;; Time-stamp: <2015-08-16 21:23:12 (michael)>

;; Package: smartparens

(use-package smartparens
  :ensure t
  :init
  (setq sp-base-key-bindings 'paredit)
  (setq sp-autoskip-closing-pair 'always)
  (setq sp-hybrid-kill-entire-symbol nil)
  :config
  (sp-use-paredit-bindings)
  (show-smartparens-global-mode +1)
  (smartparens-global-mode 1)
  (progn
    (sp-with-modes '(c-mode c++-mode java-mode sme-mode)
      (sp-local-pair "{" nil :post-handlers '(("||\n[i]" "RET")))
      (sp-local-pair "/*" "*/" :post-handlers '((" | " "SPC")
                                            ("* ||\n[i]" "RET"))))))

(provide 'setup-smartparens)

;; Time-stamp: <2015-08-11 09:28:04 (michael)>

;; Zygospore

use-package(zygospore
            :bind ("C-x 1" . zygospore-toggle-delete-other-windows))

(provide 'setup-zygospore)

;; Time-stamp: <2016-06-28 12:41:31 (michael)>
;;
;; Look and feel setup

;; Highlight closing parentheses
(show-paren-mode 1)

(setq frame-resize-pixelwise t) ; allow frame size to inc/dec by a pixel

;; MENU/TOOL/SCROLL BARS
(menu-bar-mode -1) ;; menu bar off
(tool-bar-mode -1) ;; tool bar off
(scroll-bar-mode -1) ;; scroll bars off

(setq inhibit-startup-message t ; No splash screen at startup
      scroll-step 1 ; scroll 1 line at a time
;;      tooltip-mode nil ; disable tooltip appearance on mouse hover
      blink-cursor-mode -1 ; no blinking!
      )

;; Default font
;;(when (eq system-type 'windows-nt)
;;  (set-face-attribute 'default nil
;;                      :family "Consolas"
;;                      :height 90
;;                      :weight 'normal
;;                      :width 'normal)
;;  )
;;(when (eq system-type 'darwin)
  (set-face-attribute 'default nil
                      :family "Source Code Pro"
                      :height 140
                      :weight 'normal
                      :width 'normal)
 ;; )


;; Themes
(setq custom-safe-themes t)
(use-package material-theme :ensure t)
(use-package solarized-theme :ensure t)
(use-package tango-plus-theme :ensure t)
(use-package zenburn-theme :ensure t)
(use-package tao-theme :ensure t)
(use-package warm-night-theme :ensure t)
(load-theme 'material t)
;;(load-theme 'solarized-light)
;;(load-theme 'solarized-dark)
;;(load-theme 'tango-plus t)
;;(load-theme 'zenburn t)
;;(load-theme 'tao-yang t)
;;(load-theme 'tao-yin t)
;;(load-theme 'warm-night t)

;;; Smart Mode Line
(use-package smart-mode-line
  :ensure t
  :config
  (sml/setup)
  (setq rm-blacklist ".*")           ;; No minor modes displayed
  ;;(setq rm-whitelist "Projectile") ;; Only interested in Projectile minor mode detail
  )

;; activate whitespace-mode to view all whitespace characters
(global-set-key (kbd "C-c w") 'whitespace-mode)

;; show unncessary whitespace that can mess up your diff
(add-hook 'prog-mode-hook (lambda () (interactive) (setq show-trailing-whitespace 1)))

;; Initial frame size / position
;;(when window-system
(add-to-list 'default-frame-alist '(height . 70))
(when (eq system-type 'windows-nt)
  (add-to-list 'default-frame-alist '(height . 55)))
(add-to-list 'default-frame-alist '(width . 150))
(when (eq system-type 'darwin)
  (add-to-list 'default-frame-alist '(width . 270)))
(add-to-list 'default-frame-alist '(top . 0))
(add-to-list 'default-frame-alist '(left . 0));;)

(provide 'setup-visual)
;;; setup-visual.el ends here

;; Time-stamp: <2015-08-11 15:59:52 (michael)>


(use-package yasnippet
  :ensure t
  :config
  (yas-global-mode 1))

(provide 'setup-yasnippet)

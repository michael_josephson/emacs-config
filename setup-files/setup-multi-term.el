;; Time-stamp: <2015-08-18 13:47:21 (michael)>

(use-package multi-term
  :ensure t
  :bind
  ("<f12>" . multi-term)
  :init
  (setq multi-term-program "/usr/local/bin/zsh"))


(provide 'setup-multi-term)

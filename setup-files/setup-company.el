;; Time-stamp: <2016-06-08 16:56:37 (michael)>
;;
;; Company Mode

(use-package company
  :ensure t
  :bind
  (:map c-mode-map ("<tab>" . company-complete))
  (:map c++mode-map ("<tab>" . company-complete))
  :config
  (global-company-mode))

;;(require 'company)
;;(add-hook 'after-init-hook 'global-company-mode)
;;(delete 'company-semantic company-backends)
;;(define-key c-mode-map  [(tab)] 'company-complete)
;;(define-key c++-mode-map  [(tab)] 'company-complete)
;; (define-key c-mode-map  [(control tab)] 'company-complete)
;; (define-key c++-mode-map  [(control tab)] 'company-complete)

;; company-c-headers
;;(add-to-list 'company-backends 'company-c-headers)

(provide 'setup-company)

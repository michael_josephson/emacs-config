;; Time-stamp: <2015-08-10 17:27:09 (michael)>

;; Gist
;; https://github.com/defunkt/gist.el

(use-package gist
             :commands (gist-list gist-buffer gist-region))


(provide 'setup-gist)

;; Time-stamp: <2015-08-11 13:04:45 (michael)>
;;
;; PACKAGE: anzu

;; GROUP: Editing -> Matching -> Isearch -> Anzu
;;(require 'anzu)
;;(global-anzu-mode)
;;(global-set-key (kbd "M-%") 'anzu-query-replace)
;;(global-set-key (kbd "C-M-%") 'anzu-query-replace-regexp)

(use-package anzu
  :ensure t
  :defer t
  :bind
  (("M-%" . anzu-query-replace)
   ("C-M-%" . anzu-query-replace-regexp))
  :config
  (global-anzu-mode))

(provide 'setup-anzu)

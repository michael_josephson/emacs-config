;; Time-stamp: <2015-08-11 17:01:03 (michael)>
;;
;; Miscellaneous setup

(setq gc-cons-threshold 100000000)

(windmove-default-keybindings)

(global-set-key (kbd "C-c w") 'whitespace-mode)

;; show unncessary whitespace that can mess up your diff
(add-hook 'prog-mode-hook (lambda () (interactive) (setq show-trailing-whitespace 1)))

(provide 'setup-misc)

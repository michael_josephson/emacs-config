;;; setup-elpy.el ---
;;
;; Filename: setup-elpy.el
;; Description:
;; Author: Michael Josephson
;; Maintainer:
;; Created: Wed Jun  8 13:11:18 2016 (-0400)

(use-package elpy
  :ensure t
  :config (elpy-enable)
  )

(provide 'setup-elpy)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; setup-elpy.el ends here

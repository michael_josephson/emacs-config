;; Time-stamp: <2015-08-11 13:01:06 (michael)>
;;
;; Volatile Hightlights

(use-package volatile-highlights
  :ensure t
  :config
  (volatile-highlights-mode t))

(provide 'setup-volatile-highlights)

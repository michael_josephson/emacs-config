;; Time-stamp: <2015-08-11 13:45:42 (michael)>

;; PACKAGE: comment-dwim-2

;;(global-set-key (kbd "M-;") 'comment-dwim-2)
(use-package comment-dwim-2
  :ensure t
  :bind
  ("M-;" . comment-dwim-2))

(provide 'setup-comment-dwim-2)

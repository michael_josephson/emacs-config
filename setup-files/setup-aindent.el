;; Time-stamp: <2015-08-10 16:33:16 (michael)>

;; clean-aindent-mode

;;(require 'clean-aindent-mode)
;;(add-hook 'prog-mode-hook 'clean-aindent-mode)
(use-package clean-aindent-mode
	     :ensure t
	     :bind("RET" . newline-and-indent)
	     :config
	     ((setq clean-aindent-is-simple-indent t)
	      (clean-aindent-mode t)
	      (electric-indent-mode nil)))
(provide 'setup-aindent)

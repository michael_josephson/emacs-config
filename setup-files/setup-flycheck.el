;; Time-stamp: <2016-06-08 16:32:53 (michael)>
;;
;; Flycheck

(use-package flycheck
  :ensure t
  :init   (global-flycheck-mode))

(provide 'setup-flycheck)

;; Time-stamp: <2015-08-26 13:57:38 (michael)>
;;
;; Helm

(use-package helm
  :ensure t
  :bind
  ;;(:map helm-map ("<tab>" . helm-execute-persistent-action))
  ;;(:map helm-map ("C-i" . helm-execute-persistent-action)) ; for the terminal
  ;;(:map helm-map ("C-z" . helm-select-action)) ; list actions with C-z
  ;;(:map helm-grep-mode-map ("<return>" . helm-grep-mode-jump-other-window))
  ;;(:map helm-grep-mode-map ("n" . helm-grep-mode-jump-other-window-forward))
  ;;(:map helm-grep-mode-map ("p" . helm-grep-mode-jump-other-window-backward))
  ;;(:map minibuffer-local-map ("M-p" . helm-minibuffer-history))
  ;;(:map minibuffer-local-map ("M-n" . helm-minibuffer-history))
  ("C-c h" . helm-command-prefix)
  ("M-x" . helm-M-x)
  ("M-y" . helm-show-kill-ring)
  ("C-x b" . helm-mini)
  ("C-x C-f" . helm-find-files)
  ;;("C-h SPC" . helm-all-mark-rings)
  ("C-c h o" . helm-occur)
  ("C-c h C-c w" . helm-wikipedia-suggest)
  ("C-c h x" . helm-register)
  :init
  (progn
    (global-unset-key (kbd "C-x c"))
    (require 'helm-config)
    (require 'helm-grep)
    (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)
    (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
    (define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z
    (define-key helm-grep-mode-map (kbd "<return>")  'helm-grep-mode-jump-other-window)
    (define-key helm-grep-mode-map (kbd "n")  'helm-grep-mode-jump-other-window-forward)
    (define-key helm-grep-mode-map (kbd "p")  'helm-grep-mode-jump-other-window-backward)
    (when (executable-find "curl")
      (setq helm-google-suggest-use-curl-p t))
    (setq
     helm-ff-ido-style-backspace 'always
     helm-ff-auto-update-initial-value t
     helm-ff--auto-update-state t
     helm-scroll-amount 4                    ; scroll 4 lines other window using M-<next>/M-<prior>
     helm-ff-search-library-in-sexp t        ; search for library in `require' and `declare-function' sexp.
     helm-split-window-in-side-p t           ; open helm buffer inside current window, not occupy whole other window
     helm-candidate-number-limit 500         ; limit the number of displayed canidates
     helm-google-suggest-use-curl-p t
     helm-ff-file-name-history-use-recentf t
     helm-move-to-line-cycle-in-source t     ; move to end or beginning of source when reaching top or bottom of source.
     helm-buffers-fuzzy-matching t)          ; fuzzy matching buffer names when non-nil
                                             ; useful in helm-mini that lists buffers
    (add-to-list 'helm-sources-using-default-as-input 'helm-source-man-pages)
    (define-key global-map [remap find-tag] 'helm-etags-select)
    (define-key global-map [remap list-buffers] 'helm-buffers-list)

    (with-eval-after-load 'helm-files
      (define-key helm-read-file-map (kbd "<backspace>") 'helm-find-files-up-one-level)
      (define-key helm-find-files-map (kbd "<backspace>") 'helm-find-files-up-one-level)))
  :config
  (use-package helm-swoop
    :ensure t
    :bind
    ("C-c h o" . helm-swoop)
    ("C-c s" . helm-multi-swoop-all)
    ;;(:map isearch-mode-map ("M-i" . helm-swoop-from-isearch))
    ;;(:map helm-swoop-map ("M-i" . helm-multi-swoop-all-from-helm-swoop))
    :init
    (progn
      (define-key isearch-mode-map (kbd "M-i") 'helm-swoop-from-isearch)
      ;;(define-key helm-swoop-map (kbd "M-i") 'helm-multi-swoop-all-from-helm-swoop)
      (setq helm-multi-swoop-edit-save t
            helm-swoop-split-with-multiple-windows t
            helm-swoop-split-direction 'split-window-vertically
            helm-swoop-speed-or-color t)
      (add-hook 'eshell-mode-hook
                #'(lambda ()
                    (define-key eshell-mode-map (kbd "M-l")  'helm-eshell-history)))
      (add-hook 'helm-goto-line-before-hook 'helm-save-current-pos-to-mark-ring))
  (helm-mode 1)))

(provide 'setup-helm)

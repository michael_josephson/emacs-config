;; Time-stamp: <2015-08-21 07:41:13 (michael)>

(use-package jabber
  :ensure t
  :init
  (setq jabber-auto-reconnect t)
  (setq jabber-avatar-verbose nil)
  (setq jabber-vcard-avatars-retrieve nil)
  (setq jabber-chat-buffer-format "*-jabber-%n-*")
  (setq jabber-history-enabled t)
  (setq jabber-mode-line-mode t)
  (setq jabber-roster-buffer "*-jabber-*")
  (setq jabber-roster-line-format " %c %-25n %u %-8s (%r)")
  (setq jabber-show-offline-contacts nil)
  (setq jabber-account-list
  '(("michael@mertisconsulting.com"
     (:network-server . "talk.google.com")
     (:connection-type . ssl))
    ("josephson.michael@gmail.com"
     (:network-server . "talk.google.com")
     (:connection-type . ssl))))
  :config
  (jabber-connect-all))

(provide 'setup-jabber)

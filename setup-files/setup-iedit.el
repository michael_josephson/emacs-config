;; Time-stamp: <2015-08-11 13:37:47 (michael)>

;; PACKAGE: iedit

;;(setq iedit-toggle-key-default nil)
;;(require 'iedit)
;;(global-set-key (kbd "C-;") 'iedit-mode)

(use-package iedit
  :ensure t
  :bind
  ("C-;" . iedit-mode)
  :config
  (setq iedit-toggle-key-default nil))

(provide 'setup-iedit)

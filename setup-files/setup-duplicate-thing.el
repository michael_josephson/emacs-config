;; Time-stamp: <2015-08-11 17:14:45 (michael)>

(use-package duplicate-thing
  :ensure t
  :bind
  ("M-c" . duplicate-thing))

(provide 'setup-duplicate-thing)
